<?php
require(ROOT . 'Models/Container.php');

class containerController extends Controller
{

    function create(){
        if (Controller::getPost("name")) {
            $t = new Traveler("", Controller::getPost("name"), Controller::getPost("surname"), Controller::getPost("docId"), Controller::getPost("countryId"), Controller::getPost("cargoId"));
            if (Traveler::create($t)) {
                header("Location: " . WEBROOT . "traveler/index");
            }else{
                print_r($t);
            }
        }
        $this->render("create");
    }

    function index()
    {
        $d['containers'] = Container::getAll();
        $this->set($d);
        $this->render("index");
    }


    function update($id){
        $d["traveler"] = Traveler::get($id);

        if (Controller::getPost("name")) {
            $t = new Traveler($id, Controller::getPost("name"), Controller::getPost("surname"), Controller::getPost("docId"), Controller::getPost("countryId"), Controller::getPost("cargoId"));
            if (Traveler::update($t)) {
                header("Location: " . WEBROOT . "traveler/index");
            }else{
                print_r($t);
            }
        }
        $this->set($d);
        $this->render("update");
    }

    function delete($id){
        Traveler::delete($id);
        header("Location: " . WEBROOT . "traveler/index");
    }

    /*function update($id)
    {
        $d["ship"] = Ship::get($id);

        if (isset($_POST["description"]) && isset($_POST["name"])) {
            if (Ship::update(new Ship($id, $_POST["name"], $_POST["hullCode"], $_POST["countryId"], ""))) {
                header("Location: " . WEBROOT . "ship/index");
            }
        }
        $this->set($d);
        $this->render("update");
    }

    function delete($id){
        Ship::delete($id);
        header("Location: " . WEBROOT . "ship/index");
    }

    function get($id)
    {
        header('Content-Type: application/json');
        echo json_encode(Ship::get($id));
    }*/
}