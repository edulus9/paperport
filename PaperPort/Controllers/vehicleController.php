<?php


require(ROOT . 'Models/Vehicle.php');

class vehicleController extends Controller
{

    function create(){
        if (Controller::getPost("brand")) {
            $t = new Vehicle("", Controller::getPost("brand"), Controller::getPost("model"), Controller::getPost("vin"), Controller::getPost("cargoId"), Controller::getPost("worth"));
            if (Vehicle::create($t)) {
                header("Location: " . WEBROOT . "vehicle/index");
            }else{
                print_r($t);
            }
        }
        $this->render("create");
    }

    function index()
    {
        $d['vehicles'] = Vehicle::getAll();
        $this->set($d);
        $this->render("index");
    }

    function update($id)
    {
        $d["vehicle"] = Vehicle::get($id);

        if (isset($_POST["brand"]) && isset($_POST["model"])) {
            if (Vehicle::update(new Vehicle($id, $_POST["brand"], $_POST["model"], $_POST["vin"],  $_POST["cargoId"],  $_POST["worth"], ))) {
                header("Location: " . WEBROOT . "vehicle/index");
            }
        }
        $this->set($d);
        $this->render("update");
    }

    function delete($id){
        Vehicle::delete($id);
        header("Location: " . WEBROOT . "vehicle/index");
    }

    function get($id)
    {
        header('Content-Type: application/json');
        echo json_encode(Vehicle::get($id));
    }
}