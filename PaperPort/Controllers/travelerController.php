<?php
require(ROOT . 'Models/Traveler.php');

class travelerController extends Controller
{

    function create(){
        if (Controller::getPost("name")) {
            $t = new Traveler("", Controller::getPost("name"), Controller::getPost("surname"), Controller::getPost("docId"), Controller::getPost("countryId"), Controller::getPost("cargoId"));
            if (Traveler::create($t)) {
                header("Location: " . WEBROOT . "traveler/index");
            }else{
                print_r($t);
            }
        }
        $this->render("create");
    }

    function index()
    {
        $d['travelers'] = Traveler::getAll();
        $this->set($d);
        $this->render("index");
    }


    function update($id){
        $d["traveler"] = Traveler::get($id);

        if (Controller::getPost("name")) {
            $t = new Traveler($id, Controller::getPost("name"), Controller::getPost("surname"), Controller::getPost("docId"), Controller::getPost("countryId"), Controller::getPost("cargoId"));
            if (Traveler::update($t)) {
                header("Location: " . WEBROOT . "traveler/index");
            }else{
                print_r($t);
            }
        }
        $this->set($d);
        $this->render("update");
    }

    function delete($id){
        Traveler::delete($id);
        header("Location: " . WEBROOT . "traveler/index");
    }

    function get($id)
    {
        header('Content-Type: application/json');
        echo json_encode(Traveler::get($id));
    }
}