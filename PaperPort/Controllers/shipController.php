<?php


require(ROOT . 'Models/Ship.php');

class shipController extends Controller
{

    function create(){
    }

    function index()
    {
        $d['ships'] = Ship::getAll();
        $this->set($d);
        $this->render("index");
    }

    function update($id)
    {
        $d["ship"] = Ship::get($id);

        if (isset($_POST["description"]) && isset($_POST["name"])) {
            if (Ship::update(new Ship($id, $_POST["name"], $_POST["hullCode"], $_POST["countryId"], ""))) {
                header("Location: " . WEBROOT . "ship/index");
            }
        }
        $this->set($d);
        $this->render("update");
    }

    function delete($id){
        Ship::delete($id);
        header("Location: " . WEBROOT . "ship/index");
    }

    function get($id)
    {
        header('Content-Type: application/json');
        echo json_encode(Ship::get($id));
    }
}