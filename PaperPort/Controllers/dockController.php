<?php


require(ROOT . 'Models/Dock.php');

class dockController extends Controller
{
    function index()
    {
        $d['docks'] = dock::getAll();
        $this->set($d);
        $this->render("index");
    }

    function update($id){
        $d["dock"] = Dock::get($id);

        if (isset($_POST["description"]) && isset($_POST["name"])) {
            if (Dock::update(new Dock($id, $_POST["name"], $_POST["description"]))) {
                header("Location: " . WEBROOT . "dock/index");
            }
        }
        $this->set($d);
        $this->render("update");
    }

    function get($id)
    {
        header('Content-Type: application/json');
        echo json_encode(Dock::get($id));
    }
}