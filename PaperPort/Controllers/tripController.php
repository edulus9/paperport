<?php
require(ROOT . 'Models/Trip.php');
require(ROOT . 'Models/Cargo.php');
require(ROOT . 'Models/Vehicle.php');
require(ROOT . 'Models/Container.php');
require(ROOT . 'Models/Traveler.php');
require_once(ROOT . 'Core/Controller.php');
class tripController extends Controller
{
    function index()
    {

        $d['trips'] = Trip::getAll();
        $this->set($d);
        $this->render("index");
    }

    function get($id){
        header('Content-Type: application/json');
        echo json_encode(Trip::get($id));
    }

    function create(){

        if (Controller::getPost("shipId")) {
            $t = new Trip("", Controller::getPost("arrival"), Controller::getPost("departure"), Controller::getPost("estArrival"), Controller::getPost("estDeparture"), Controller::getPost("countryId"), Controller::getPost("dockId"), Controller::getPost("execution"), isset($_POST["incoming"]), Controller::getPost("shipId"));
            if (Trip::create($t)) {
                header("Location: " . WEBROOT . "trip/index");
            }else{
                print_r($t);
            }
        }
        $this->render("create");
    }

    function update($id){
        $d["trip"] = Trip::get($id);

        if (Controller::getPost("shipId")) {
            $t = new Trip($id, Controller::getPost("arrival"), Controller::getPost("departure"), Controller::getPost("estArrival"), Controller::getPost("estDeparture"), Controller::getPost("countryId"), Controller::getPost("dockId"), Controller::getPost("execution"), isset($_POST["incoming"]), Controller::getPost("shipId"));
            if (Trip::update($t)) {
                header("Location: " . WEBROOT . "trip/index");
            }else{
                print_r($t);
            }
        }
        $this->set($d);
        $this->render("update");
    }

    function calendar(){
        if(isset($_POST["day"])){

        }else{
            $_POST["day"]=date("Y-m-d");
        }

        $d['trips'] = Trip::getByDay($_POST["day"]);
        $this->set($d);


        $this->render("calendar");
    }

    function getOperationalData($id){

        $cargoIds=Cargo::getIdsFromTrip($id);

        $l = [
            "nVehicles"=>Vehicle::getQuantityForCargos($cargoIds)["COUNT(id)"],
            "nContainers"=>Container::getQuantityForCargos($cargoIds)["COUNT(id)"],
            "nPeople"=>Traveler::getQuantityForCargos($cargoIds)["COUNT(id)"],
        ];
        header('Content-Type: application/json');
        echo json_encode($l);
    }

    function delete($id){
        Trip::delete($id);
        header("Location: " . WEBROOT . "trip/index");
    }

}