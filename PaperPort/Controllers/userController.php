<?php
require_once(ROOT . 'Models/User.php');
class userController extends Controller
{
    function index()
    {
        $d['users'] = User::getAll();
        $this->set($d);
        $this->render("index");
    }

    function login()
    {
        if (isset($_POST["name"]) && isset($_POST["password"])) {

            $user = new User("", $_POST["name"], $_POST["password"]);


            if ($r = $user->login($_POST["name"], $_POST["password"])) {
                $_SESSION["authName"] = $r["name"];
                $_SESSION["authToken"] = $user->getToken($r["passwordHASH"]);
                $_SESSION["id"]=$r["id"];
                unset($_POST["error"]);
                header("Location: " . WEBROOT . "trip/index");
            }else $_POST["error"]="Password or username incorrect.";
        }

        $this->render("login");
    }

    function register()
    {
        if (isset($_POST["name"]) && isset($_POST["password"])) {

            $user = new User("", $_POST["name"], $_POST["password"]);

            if ($r = $user->register($_POST["name"], $_POST["password"])) {
                //setcookie("authToken", $r["name"]);
                unset($_POST["error"]);
                header("Location: " . WEBROOT . "user/index");
            } else {
                $_POST["error"] = "This user has already been registered here...";
            }
        }

        $this->render("register");
    }

    public function exit()
    {
        User::exit();
        header("Location: " . WEBROOT . "user/login");
    }
}