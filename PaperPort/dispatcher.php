<?php
require_once ROOT."/Models/User.php";

class Dispatcher
{

    private $request;

    public function dispatch()
    {
        session_start();
        $this->request = new Request();

        if (!isset($_SESSION["authToken"]) && !isset($_SESSION["authName"]) && $this->request->url != "/PaperPort/user/login") {
            /*Router::parse($this->request->url, $this->request);

            $controller = $this->loadController();

            call_user_func_array([$controller, $this->request->action], $this->request->params);
            ob_start();*/
            header('Location: ' . "/PaperPort/user/login");
            //ob_end_flush();
            return;
        }
        
        if(!(@User::checkToken($_SESSION["authName"], $_SESSION["authToken"])) && $this->request->url != "/PaperPort/user/login" && $this->request->url != "/PaperPort/user/register"){
            User::exit();
            ob_start();
            header('Location: ' . "/PaperPort/user/login");
            ob_end_flush();
            return;
        }

        Router::parse($this->request->url, $this->request);

        $controller = $this->loadController();

        call_user_func_array([$controller, $this->request->action], $this->request->params);
    }

    public function loadController()
    {
        $name = $this->request->controller . "Controller";
        $file = ROOT . 'Controllers/' . $name . '.php';
        //echo $file;
        require($file);
        $controller = new $name();
        return $controller;
    }

}

?>