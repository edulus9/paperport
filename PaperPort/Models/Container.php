<?php
class Container extends Model
{
    public $id, $cargoId, $containerId, $description, $worth;

    /**
     * Container constructor.
     * @param $id
     * @param $cargoId
     * @param $containerId
     * @param $description
     * @param $worth
     */
    public function __construct($id, $cargoId, $containerId, $description, $worth)
    {
        $this->id = $id;
        $this->cargoId = $cargoId;
        $this->containerId = $containerId;
        $this->description = $description;
        $this->worth = $worth;
    }

    static function get($id){
        $sql = "SELECT * FROM Container where Id=:id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "id"=> $id,
        ]);
        $result= $req->fetch();

        return self::fromFetch($result);
    }

    static function getAll(){
        $sql = "SELECT * FROM Container";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result= $req->fetchAll();

        for($i=0; $i<count($result); $i++){
            $l[$i]=self::fromFetch($result[$i]);
        }

        return $l;
    }

    /*static function update($ship){
        $sql = "UPDATE Ships SET Name = :name, HullCode = :hullCode, CountryId= :countryId WHERE Id = :id";

        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'id' => $ship->id,
            'title' => $ship->name,
            'hullCode' => $ship->hullCode,
            'countryId' => $ship->countryId
        ]);
    }

    static function delete($id){
        $sql = 'UPDATE Ships SET deleted = 1 WHERE id = :id';
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'id'=>$id
        ]);
    }*/

    private static function fromFetch($result){
        return new Container(
            $result['id'],
            $result['cargoId'],
            $result['containerId'],
            $result['description'],
            $result['worth']
        );
    }

    static function getQuantityForCargos($cargoIds){
        $sql = "SELECT COUNT(id) FROM Container where FALSE ";

        for ($i=0; $i<count($cargoIds); $i++) {
            $val=$cargoIds[$i];
            $sql= $sql . " OR cargoId=${val[0]}";
        }
        $req = Database::getBdd()->prepare($sql);
        $req->execute();

        $result= $req->fetch();
        return $result;
    }
}
?>