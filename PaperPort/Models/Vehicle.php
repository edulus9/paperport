<?php
class Vehicle extends Model
{
    public $id, $brand, $model, $vin, $cargoId, $worth;

    /**
     * Vehicle constructor.
     * @param $id
     * @param $brand
     * @param $model
     * @param $vin
     * @param $cargoId
     * @param $worth
     */
    public function __construct($id, $brand, $model, $vin, $cargoId, $worth)
    {
        $this->id = $id;
        $this->brand = $brand;
        $this->model = $model;
        $this->vin = $vin;
        $this->cargoId = $cargoId;
        $this->worth = $worth;
    }

    static function get($id){
        $sql = "SELECT * FROM Vehicle where id=:id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "id"=> $id,
        ]);
        $result= $req->fetch();

        return self::fromFetch($result);
    }

    static function getAll(){
        $sql = "SELECT * FROM Vehicle";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result= $req->fetchAll();

        for($i=0; $i<count($result); $i++){
            $l[$i]=self::fromFetch($result[$i]);
        }

        return $l;
    }

    static function update($vehicle){
        $sql = "UPDATE Vehicle SET Brand = :brand, Model = :model, cargoId= :cargoId, VIN = :vin, worth= :worth WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'id' => $vehicle->id,
            'model' => $vehicle->model,
            'brand' => $vehicle->brand,
            'worth' => $vehicle->worth,
            'cargoId' => $vehicle->cargoId,
            'vin' => $vehicle->vin,
        ]);
    }

    static function delete($id){
        $sql = 'DELETE FROM `Vehicle` WHERE `Vehicle`.`id` = :id';
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'id'=>$id
        ]);
    }

    static function getQuantityForCargos($cargoIds){
        $sql = "SELECT COUNT(id) FROM Vehicle where FALSE ";

        for ($i=0; $i<count($cargoIds); $i++) {
            $val=$cargoIds[$i];
            $sql= $sql . " OR cargoId=${val[0]}";
        }
        $req = Database::getBdd()->prepare($sql);
        $req->execute();

        $result= $req->fetch();
        return $result;
    }

    static function create($vehicle){

        $sql ="INSERT INTO `Vehicle` (brand, model, vin, worth, cargoId) VALUES " .
            "(:brand, :model, :vin, :worth, :cargoId)";
        $req = Database::getBdd()->prepare($sql);

        $res = $req->execute([
            'brand' => ($vehicle->brand) ? $vehicle->brand : NULL,
            'model' => ($vehicle->model) ? $vehicle->model : NULL,
            'vin' => ($vehicle->vin) ? $vehicle->vin : NULL,
            'worth' => ($vehicle->worth) ? $vehicle->worth : NULL,
            'cargoId' => ($vehicle->cargoId) ? $vehicle->cargoId : NULL,
        ]);

        print_r($req->errorInfo());
        return $res;
    }

    private static function fromFetch($result){
        return new Vehicle(
            $result['id'],
            $result['Brand'],
            $result['Model'],
            $result['VIN'],
            $result['cargoId'],
            $result['worth']
        );
    }


}
?>