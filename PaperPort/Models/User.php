<?php

class User extends Model{

    public $id, $name;
    private $passwordHASH;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $passwordHASH
     */
    public function __construct($id, $name, $passwordHASH)
    {
        $this->id = $id;
        $this->name = $name;
        $this->passwordHASH = $passwordHASH;
    }


    public static function getAll(){
        $sql = "SELECT * FROM Users";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result= $req->fetchAll();

        for($i=0; $i<count($result); $i++){
            $l[$i]=self::fromFetch($result[$i]);
        }

        return $l;
    }

    public static function login($name, $password){
        $passwordHASH= md5($password);
        $name=mb_strtolower($name);

        $sql = "SELECT * FROM Users WHERE name = :name AND passwordHASH=:passwordHASH";
        $result=Database::getBdd()->prepare($sql);
        $result->execute([
            "name"=> $name,
            "passwordHASH"=>$passwordHASH,
        ]);

        $result= $result->fetch();
        return $result;
    }

    public function register($name, $password){
        $passwordHASH= md5($password);
        $name=mb_strtolower($name);

        if(!$this->userExists($name)) {
            $sql = "INSERT INTO Users(name, passwordHASH) VALUES (:name, :passwordHASH)";
            $sqlGet = "SELECT * FROM Users WHERE name = :name";
            $req = Database::getBdd()->prepare($sql);

            $res = $req->execute([
                'name' => $name,
                'passwordHASH' => $passwordHASH,
            ]);
            echo "name: $name; pass: $passwordHASH";

            $get = Database::getBdd()->prepare($sqlGet);
            $get->execute([
                'name' => $name,
            ]);
            return $get->fetch();
        }
        return false;
    }

    private function userExists($name){
        $sql = "SELECT * FROM Users WHERE name=:name";
        $sqlCheckResult=Database::getBdd()->prepare($sql);
        $sqlCheckResult->execute([
            'name' => $name,
        ]);
        $res=$sqlCheckResult->fetch();
        return $res;
    }

    public function getToken($s){
        return md5($s);
    }

    public static function checkToken($authName, $authToken)
    {
        $sql = "SELECT * FROM Users WHERE name = :name";
        $result=Database::getBdd()->prepare($sql);
        $result->execute([
            "name"=> $authName,
        ]);

        $result= $result->fetch();

        return ($authToken==md5($result["passwordHASH"]));

    }

    public static function exit(){
        unset($_SESSION["authName"]);
        unset($_SESSION["authToken"]);
        session_unset();
        if(session_id()) session_destroy();
    }



    private static function fromFetch($result){
        return new User(
            $result['id'],
            $result['name'],
            $result['passwordHASH'],
        );
    }

}
