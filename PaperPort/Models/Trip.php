<?php

class Trip extends Model
{

    public $id, $arrival, $departure, $estArrival, $estDeparture, $countryId, $dockId, $execution, $incoming, $shipId;

    /**
     * Trip constructor.
     * @param $id
     * @param $arrival
     * @param $departure
     * @param $estArrival
     * @param $estDeparture
     * @param $countryId
     * @param $dockId
     * @param $execution
     * @param $incoming
     * @param $shipId
     */
    public function __construct($id, $arrival, $departure, $estArrival, $estDeparture, $countryId, $dockId, $execution, $incoming, $shipId)
    {
        $this->id = $id;
        $this->arrival = $arrival;
        $this->departure = $departure;
        $this->estArrival = $estArrival;
        $this->estDeparture = $estDeparture;
        $this->countryId = $countryId;
        $this->dockId = $dockId;
        $this->execution = $execution;
        $this->incoming = $incoming;
        $this->shipId = $shipId;
    }

    static function getAll()
    {
        $sql = "SELECT * FROM Trip";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result = $req->fetchAll();

        for ($i = 0; $i < count($result); $i++) {
            $l[$i] = self::fromFetch($result[$i]);
        }

        return $l;
    }

    static function getByDay($day)
    {
        $date=new DateTime($day);
        $date->modify("+1 days");
        $start=$day;
        $end=$date->format('Y-m-d');
        $sql = "SELECT * FROM Trip WHERE (EstimatedArrival>:start AND EstimatedArrival<:end) AND (EstimatedDeparture>:start AND EstimatedDeparture<:end) OR (EstimatedArrival>:start AND EstimatedArrival<:end) OR (EstimatedDeparture>:start AND EstimatedDeparture<:end)";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "start"=>$start,
            "end"=>$end,
        ]);
        $l = [];
        $result = $req->fetchAll();

        for ($i = 0; $i < count($result); $i++) {
            $l[$i] = self::fromFetch($result[$i]);
        }

        return $l;
    }

    private static function fromFetch($value)
    {
        return new Trip(
            $value["id"],
            $value["Arrival"],
            $value["Departure"],
            $value["EstimatedArrival"],
            $value["EstimatedDeparture"],
            $value["CountryId"],
            $value["DockId"],
            $value["Execution"],
            $value["Incoming"],
            $value["ShipId"]
        );
    }

    static function get($id)
    {
        $sql = "SELECT * FROM Trip WHERE Id=:id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "id" => $id
        ]);
        $l = [];
        $result = $req->fetch();
        return self::fromFetch($result);
    }

    static function update($trip)
    {
        $sql = "UPDATE Trip SET Arrival = :arrival, Departure= :departure,  EstimatedArrival = :estArrival,  EstimatedDeparture = :estDeparture,  Execution = :execution,  ShipId = :shipId,  DockId = :dockId, Incoming = :incoming, CountryId = :countryId WHERE id = :id";
        $req = Database::getBdd()->prepare($sql);

        $res = $req->execute([
            'id' => $trip->id,
            'arrival' => ($trip->arrival) ? $trip->arrival : NULL,
            'departure' => ($trip->departure) ? $trip->departure : NULL,
            'estArrival' => ($trip->estArrival) ? $trip->estArrival : NULL,
            'estDeparture' => ($trip->estDeparture) ? $trip->estDeparture : NULL,
            'execution' => ($trip->execution) ? $trip->execution : NULL,
            'shipId' => $trip->shipId,
            'dockId' => $trip->dockId,
            'incoming' => ($trip->incoming) ? 1 : 0,
            'countryId' => $trip->countryId,
        ]);

        print_r($req->errorInfo());

        return $res;
    }

    static function create($trip)
    {
        $sql ="INSERT INTO `Trip` (`Arrival`, `Departure`, `EstimatedArrival`, `EstimatedDeparture`, `ShipId`, `CountryId`, `Incoming`, `Execution`, `DockId`) VALUES " .
            "(:arrival, :departure, :estArrival, :estDeparture, :shipId, :countryId, :incoming, :execution, :dockId)";
        $req = Database::getBdd()->prepare($sql);

        $res = $req->execute([
            'arrival' => ($trip->arrival) ? $trip->arrival : NULL,
            'departure' => ($trip->departure) ? $trip->departure : NULL,
            'estArrival' => ($trip->estArrival) ? $trip->estArrival : NULL,
            'estDeparture' => ($trip->estDeparture) ? $trip->estDeparture : NULL,
            'execution' => ($trip->execution) ? $trip->execution : NULL,
            'shipId' => $trip->shipId,
            'dockId' => $trip->dockId,
            'countryId' => $trip->countryId,
            'incoming' => ($trip->incoming) ? 1 : 0,
        ]);

        print_r($req->errorInfo());
        echo $req->queryString;
        return $res;
    }

    static function delete($id){
        $sql = "DELETE FROM `Trip` WHERE `Trip`.`id` = :id";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            "id" => $id
        ]);
    }
}
