<?php
class Ship extends Model
{
    public $id, $name, $hullCode, $countryId, $added;

    /**
     * Ship constructor.
     * @param $id
     * @param $name
     * @param $hullCode
     * @param $countryId
     * @param $added
     */
    public function __construct($id, $name, $hullCode, $countryId, $added)
    {
        $this->id = $id;
        $this->name = $name;
        $this->hullCode = $hullCode;
        $this->countryId = $countryId;
        $this->added = $added;
    }

    static function get($id){
        $sql = "SELECT * FROM Ships where Id=:id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "id"=> $id,
        ]);
        $result= $req->fetch();

        return self::fromFetch($result);
    }

    static function getAll(){
        $sql = "SELECT * FROM Ships WHERE Deleted=0";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result= $req->fetchAll();

        for($i=0; $i<count($result); $i++){
            $l[$i]=self::fromFetch($result[$i]);
        }

        return $l;
    }

    static function update($ship){
        $sql = "UPDATE Ships SET Name = :name, HullCode = :hullCode, CountryId= :countryId WHERE Id = :id";

        $req = Database::getBdd()->prepare($sql);
        $v= $req->execute([
            'id' => $ship->id,
            'name' => $ship->name,
            'hullCode' => $ship->hullCode,
            'countryId' => $ship->countryId
        ]);

        print_r($req->errorInfo());
        return $v;
    }

    static function delete($id){
        $sql = 'UPDATE Ships SET deleted = 1 WHERE id = :id';
        $req = Database::getBdd()->prepare($sql);
        $v= $req->execute([
            'id'=>$id
        ]);

        print_r($req->errorInfo());
        return $v;
    }

    private static function fromFetch($result){
        return new Ship(
            $result['Id'],
            $result['Name'],
            $result['HullCode'],
            $result['CountryId'],
            $result['added']
        );
    }
}
?>