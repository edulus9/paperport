<?php
class Traveler extends Model
{
    public $id, $name, $surname, $docId, $countryId, $cargoId;

    /**
     * Traveler constructor.
     * @param $id
     * @param $name
     * @param $surname
     * @param $docId
     * @param $countryId
     * @param $cargoId
     */
    public function __construct($id, $name, $surname, $docId, $countryId, $cargoId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->docId = $docId;
        $this->countryId = $countryId;
        $this->cargoId = $cargoId;
    }


    static function get($id){
        $sql = "SELECT * FROM Traveler where id=:id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "id"=> $id,
        ]);
        $result= $req->fetch();

        return self::fromFetch($result);
    }

    static function update($traveler){
        $sql = "UPDATE Traveler SET name = :name, surname = :surname, docId = :docId, countryId= :countryId, cargoId= :cargoId WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);
        $v= $req->execute([
            'id' => $traveler->id,
            'name' => $traveler->name,
            'surname' => $traveler->surname,
            'docId' => $traveler->docId,
            'countryId' => $traveler->countryId,
            'cargoId' => $traveler->cargoId,
        ]);

        print_r($req->errorInfo());
        return $v;
    }

    static function create($traveler){

        $sql ="INSERT INTO `Traveler` (name, surname, docId, countryId, cargoId) VALUES " .
            "(:name, :surname, :docId, :countryId, :cargoId)";
        $req = Database::getBdd()->prepare($sql);

        $res = $req->execute([
            'name' => ($traveler->name) ? $traveler->name : NULL,
            'surname' => ($traveler->surname) ? $traveler->surname : NULL,
            'docId' => ($traveler->docId) ? $traveler->docId : NULL,
            'countryId' => ($traveler->countryId) ? $traveler->countryId : NULL,
            'cargoId' => ($traveler->cargoId) ? $traveler->cargoId : NULL,

        ]);

        print_r($req->errorInfo());
        return $res;
    }

    static function getAll(){
        $sql = "SELECT * FROM Traveler";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result= $req->fetchAll();

        for($i=0; $i<count($result); $i++){
            $l[$i]=self::fromFetch($result[$i]);
        }

        return $l;
    }

    /*static function update($vehicle){
        $sql = "UPDATE Vehicle SET Name = :name, HullCode = :hullCode, CountryId= :countryId WHERE Id = :id";

        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'id' => $vehicle->id,
            'title' => $vehicle->name,
            'hullCode' => $vehicle->hullCode,
            'countryId' => $vehicle->countryId
        ]);
    }
*/
    static function delete($id){
        $sql = "DELETE FROM `Traveler` WHERE `Traveler`.`id` = :id";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            "id" => $id
        ]);
    }



    static function getQuantityForCargos($cargoIds){
        $sql = "SELECT COUNT(id) FROM Traveler where FALSE ";

        for ($i=0; $i<count($cargoIds); $i++) {
            $val=$cargoIds[$i];
            $sql= $sql . " OR cargoId=${val[0]}";
        }
        $req = Database::getBdd()->prepare($sql);
        $req->execute();

        $result= $req->fetch();
        return $result;
    }

    private static function fromFetch($result){
        return new Traveler(
            $result['id'],
            $result['name'],
            $result['surname'],
            $result['docId'],
            $result['countryId'],
            $result['cargoId']
        );
    }


}
?>