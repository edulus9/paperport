<?php
class Dock extends Model
{
    public $id, $name, $description;

    /**
     * Dock constructor.
     * @param $id
     * @param $name
     * @param $description
     */
    public function __construct($id, $name, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    static function getAll(){
        $sql = "SELECT * FROM Docks";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result= $req->fetchAll();

        for($i=0; $i<count($result); $i++){
            $l[$i]=self::fromFetch($result[$i]);
        }

        return $l;
    }

    static function get($id){
        $sql = "SELECT * FROM Docks where id=:id";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "id" => $id,
        ]);
        $result= $req->fetch();

        return self::fromFetch($result);
    }

    static function update($dock){
        $sql = "UPDATE Docks SET name = :title, description = :description WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);
        return $req->execute([
            'id' => $dock->id,
            'title' => $dock->name,
            'description' => $dock->description
        ]);
    }

    private static function fromFetch($result){
        return new Dock(
            $result['id'],
            $result['name'],
            $result['description'],
        );
    }
}
?>