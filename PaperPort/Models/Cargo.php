<?php

class Cargo extends Model{

    public $id, $notes, $tripId;

    /**
     * Cargo constructor.
     * @param $id
     * @param $notes
     * @param $tripId
     */
    public function __construct($id, $notes, $tripId)
    {
        $this->id = $id;
        $this->notes = $notes;
        $this->tripId = $tripId;
    }


    static function getAll(){
        $sql = "SELECT * FROM Cargo";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        $l = [];
        $result = $req->fetchAll();

        for ($i = 0; $i < count($result); $i++) {
            $l[$i] = self::fromFetch($result[$i]);
        }

        return $l;
    }


    private static function fromFetch($value)
    {
        return new Cargo(
            $value["Id"],
            $value["notes"],
            $value["tripId"],
        );
    }

    public static function getIdsFromTrip($tripId){
        $sql = "SELECT id FROM Cargo where TripId=:tripId";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            "tripId"=> $tripId,
        ]);
        return $req->fetchAll();
    }

}
