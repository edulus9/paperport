# PaperPort Software

The Paperopoli Port Authority commissioned the development of a Terminal Operating System. It provides (or should provide) maximum reliability and complies with what has been laid down in the “Terminal-01” file.

The website is hosted by Altervista and supportes HTTPS and Cloudflare. 

It is available [here](https://paperport.altervista.org/PaperPort/trip/index)