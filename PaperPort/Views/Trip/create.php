<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="../../../js/jquery.datetimepicker.css"/>
<style type="text/css">

    .custom-date-style {
        background-color: red !important;
    }

    .input{
    }
    .input-wide{
        width: 500px;
    }

</style>
<body>
<h1>Create Trip </h1>
<form method='post' action='#'>
    <div class="form-group">

        <label for="title">dockId</label>
        <input type="text" class="form-control" id="dockId" placeholder="Enter dockId" name="dockId"
               value="<?php if (isset($trip->dockId)) echo $trip->dockId; ?>">
        <label for="title">Incoming</label>
        <input type="checkbox" class="form-control" id="incoming" placeholder="" name="incoming"
               checked="<?php if (isset($trip->incoming)) echo $trip->incoming; ?>">
    </div>

    <div class="form-group">
        <label for="description">shipId</label>
        <input type="text" class="form-control" id="shipId" placeholder="Enter shipId" name="shipId"
               value="<?php if (isset($trip->shipId)) echo $trip->shipId; ?>">
    </div>
    <div class="form-group">
        <label for="description">countryId</label>
        <input type="text" class="form-control" id="countryId" placeholder="Enter countryId" name="countryId"
               value="<?php if (isset($trip->countryId)) echo $trip->countryId; ?>">
    </div>

    <label for="description">arrival</label> <input type="text" value="<?php if (isset($trip->arrival)) echo $trip->arrival; ?>" id="arrival" name="arrival"/><br>
    <label for="description">departure</label> <input type="text" value="<?php if (isset($trip->departure)) echo $trip->departure; ?>" id="departure" id="departure" name="departure"/><br>
    <label for="description">estArrival</label> <input type="text" value="<?php if (isset($trip->estArrival)) echo $trip->estArrival; ?>" id="estArrival" id="estArrival" name="estArrival"/><br>
    <label for="description">estDeparture</label> <input type="text" value="<?php if (isset($trip->estDeparture)) echo $trip->estDeparture; ?>" id="estDeparture" id="estDeparture" name="estDeparture"/><br>
    <label for="description">execution</label> <input type="text" value="<?php if (isset($trip->execution)) echo $trip->execution; ?>" id="execution" id="execution" name="execution"/><br><br>
    <button type="submit" class="btn btn-primary">Submit</button>
</body>
<script src="../../../js/jquery.js"></script>
<script src="../../../js/node_modules/php-date-formatter/js/php-date-formatter.min.js"></script>
<script src="../../../js/node_modules/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../../../js/jquery.datetimepickeroo.js"></script>
<script>

    function onSubmit(){

    }


    $('#arrival').datetimepicker({
        formatTime:'H:i:s',
        formatDate:'d.m.Y',
        format:'Y-m-d H:i:s',
        dayOfWeekStart : 1,
        lang:'en',
        startDate:	'<?php if (isset($trip->arrival)) echo $trip->arrival; ?>'
    });
    $('#departure').datetimepicker({
        formatTime:'H:i:s',
        formatDate:'d.m.Y',
        format:'Y-m-d H:i:s',
        dayOfWeekStart : 1,
        lang:'en',
        startDate:	'<?php if (isset($trip->departure)) echo $trip->departure; ?>'
    });
    $('#estArrival').datetimepicker({
        formatTime:'H:i:s',
        formatDate:'d.m.Y',
        format:'Y-m-d H:i:s',
        dayOfWeekStart : 1,
        lang:'en',
        startDate:	'<?php if (isset($trip->estArrival)) echo $trip->estArrival; ?>'
    });
    $('#estDeparture').datetimepicker({
        formatTime:'H:i:s',
        formatDate:'d.m.Y',
        format:'Y-m-d H:i:s',
        dayOfWeekStart : 1,
        lang:'en',
        startDate:	'<?php if (isset($trip->estDeparture)) echo $trip->estDeparture; ?>'
    });
    $('#execution').datetimepicker({
        formatTime:'H:i:s',
        formatDate:'d.m.Y',
        format:'Y-m-d H:i:s',
        dayOfWeekStart : 1,
        lang:'en',
        startDate:	'<?php if (isset($trip->execution)) echo $trip->execution; ?>'
    });
    /*var logic = function( currentDateTime ){
        if (currentDateTime && currentDateTime.getDay() == 6){
            this.setOptions({
                minTime:'11:00'
            });
        }else
            this.setOptions({
                minTime:'8:00'
            });
    };
    var dateToDisable = new Date();
    dateToDisable.setDate(dateToDisable.getDate() + 2);*/
</script>
</form>