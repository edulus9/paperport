<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" type="text/css" href="../../../js/jquery.datetimepicker.css"/>

<style type="text/css">

    .custom-date-style {
        background-color: red !important;
    }

    .input{
    }
    .input-wide{
        width: 500px;
    }

</style>
<body>

<form method='post' action='#' id="calendarForm">
    <h1><img src="../../../images/calendar.png" height="45"> Calendar <!----></h1>
    Choose day to show trips

    <div id="">
        <input type="text" value="<?php if (isset($trip->arrival)) echo $trip->arrival; ?>" id="day" name="day"/>

        <table class="table table-striped custab">
                    <thead>
                    <tr id="tableElements">
                        <th>ID</th>
                        <th>Ship</th>
                        <th>Country</th>
                        <th>Est. Arr.</th>
                        <th>Est. Dep.</th>
                        <th>DockId</th>
                    </tr>
                    </thead>
                    <?php
                    require_once("../Models/Ship.php");
                    require_once("../Models/Dock.php");
                    $inbound="";
                    foreach ($trips as $trip)
                    {
                        $inbound=($trip->incoming) ? '#98fb98' : '#ff8080';
                        echo '<tr >';

                        echo "<td bgcolor=$inbound>" . $trip->id . "</td>";
                        //echo "<td bgcolor=$inbound><a href='/PaperPort/ship/get/{$trip->id}'>" . Ship::get($trip->shipId)->name . "</a></td>";
                        echo "<td bgcolor=$inbound><button class='btn btn-info btn-xs' onclick=showShip('/PaperPort/ship/get/{$trip->shipId}')>" . Ship::get($trip->shipId)->name . "</button></td>";
                        echo "<td bgcolor=$inbound>" . $trip->countryId . "</td>";
                        echo "<td bgcolor=$inbound>" . $trip->estArrival . "</td>";
                        echo "<td bgcolor=$inbound>" . $trip->estDeparture . "</td>";
                        echo "<td bgcolor=$inbound><button class='btn btn-info btn-xs' onclick=showDock('/PaperPort/dock/get/{$trip->dockId}') href='/PaperPort/dock/get/{$trip->dockId}'>" . Dock::get($trip->dockId)->name . "</button></td>";

                        echo "</tr>";

                    }
                    ?>
                </table>
        </div>


</body>
<script src="../../../js/jquery.js"></script>
<script src="../../../js/node_modules/php-date-formatter/js/php-date-formatter.min.js"></script>
<script src="../../../js/node_modules/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="../../../js/jquery.datetimepickeroo.js"></script>
<script>

    function onSubmit(){
        console.log("ckjdbfkjdf");
    }

    function showDock(link){
        var http = new XMLHttpRequest();
        var url = link;
        var params = '';
        http.open('POST', url, true);

        //Send the proper header information along with the request
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var v=JSON.parse(http.responseText), s="";

                s+="id: "+v.id;
                s+="\nname: "+v.name;
                s+="\ndescription: "+v.description;
                alert(s);
            }
        }
        http.send(params);
    }

    function showShip(link){
        var http = new XMLHttpRequest();
        var url = link;
        var params = '';
        http.open('POST', url, true);

        //Send the proper header information along with the request
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var v=JSON.parse(http.responseText), s="";

                s+="id: "+v.id;
                s+="\nname: "+v.name;
                s+="\nhullCode: "+v.hullCode;
                s+="\ncountry: "+v.countryId;
                alert(s);
            }
        }
        http.send(params);
    }

    $('#day').datetimepicker({
        inline: true,
        timepicker: false,
        formatDate:'d.m.Y',
        format:'Y-m-d',
        dayOfWeekStart : 1,
        lang:'en',
        value:	'<?php if (isset($_POST["day"])) echo $_POST["day"]; else echo date("Y-m-d")?>',
        onChangeDateTime:function(dp,$input){
            console.log($input.val() + "kjsdbfkdsbfkjbd")
            document.getElementById("calendarForm").submit();
        }
    });
    /*var logic = function( currentDateTime ){
        if (currentDateTime && currentDateTime.getDay() == 6){
            this.setOptions({
                minTime:'11:00'
            });
        }else
            this.setOptions({
                minTime:'8:00'
            });
    };
    var dateToDisable = new Date();
    dateToDisable.setDate(dateToDisable.getDate() + 2);*/
</script>
</form>