<script>

    function showOperationalData(id){

        var http = new XMLHttpRequest();
        var url = 'getOperationalData/' + id;
        var params = '<?php if (isset($task["title"])) echo "id=".$task["id"];?>';
        http.open('POST', url, true);

        //Send the proper header information along with the request
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                document.getElementById("vehicles").hidden=document.getElementById("containers").hidden=document.getElementById("people").hidden=false;

                document.getElementById("vehicles" + id).hidden=false;
                document.getElementById("vehicles" + id).innerText= JSON.parse(http.responseText)["nVehicles"];
                document.getElementById("containers" + id).hidden=false;
                document.getElementById("containers" + id).innerText= JSON.parse(http.responseText)["nContainers"];
                document.getElementById("people" + id).hidden=false;
                document.getElementById("people" + id).innerText= JSON.parse(http.responseText)["nPeople"];
            }
        }
        http.send(params);
    }
</script>

<h1><img src="../../../images/trip.png" height="45"> Trips</h1>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
        <a href="/PaperPort/trip/create/" class="btn btn-primary btn-xs pull-right"><b>+</b></a>
        <tr id="tableElements">
            <th>ID</th>
            <th>Ship</th>
            <th>Country</th>
            <th>Arrival</th>
            <th>Departure</th>
            <th>Est. Arr.</th>
            <th>Est. Dep.</th>
            <th>DockId</th>
            <th>Execution</th>
            <th class="text-center">Action</th>
            <th bgcolor="#6495ed" id="vehicles" hidden>Vehicles</th>
            <th bgcolor="#6495ed" id="containers" hidden>Containers</th>
            <th bgcolor="#6495ed" id="people" hidden>People</th>
        </tr>
        </thead>
        <?php
        require_once("../Models/Ship.php");
        require_once("../Models/Dock.php");
        $inbound="";
        foreach ($trips as $trip)
        {

            $inbound=($trip->incoming) ? '#98fb98' : '#ff8080';
            echo '<tr >';

            echo "<td bgcolor=$inbound>" . $trip->id . "</td>";
            echo "<td bgcolor=$inbound><button class='btn btn-info btn-xs' onclick=showShip('/PaperPort/ship/get/{$trip->shipId}')>" . Ship::get($trip->shipId)->name . "</button></td>";
            echo "<td bgcolor=$inbound>" . $trip->countryId . "</td>";
            echo "<td bgcolor=$inbound>" . $trip->arrival . "</td>";
            echo "<td bgcolor=$inbound>" . $trip->departure . "</td>";
            echo "<td bgcolor=$inbound>" . $trip->estArrival . "</td>";
            echo "<td bgcolor=$inbound>" . $trip->estDeparture . "</td>";
            echo "<td bgcolor=$inbound><button class='btn btn-info btn-xs' onclick=showDock('/PaperPort/dock/get/{$trip->dockId}') href='/PaperPort/dock/get/{$trip->dockId}'>" . Dock::get($trip->dockId)->name . "</button></td>";
            echo "<td bgcolor=$inbound>" . $trip->execution . "</td>";
            echo "<td bgcolor=$inbound class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/trip/update/" . $trip->id . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a>";
            echo "<button class='btn btn-info btn-xs' onclick='showOperationalData($trip->id)'><span class='glyphicon glyphicon-edit'></span> Stats</button>";

            echo "<a href='/PaperPort/trip/delete/" . $trip->id . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";

            echo "<td bgcolor='#00bfff' hidden id='vehicles". $trip->id ."' bgcolor=$inbound>" . $trip->execution . "</td>";
            echo "<td bgcolor='#00bfff' hidden id='containers". $trip->id ."' bgcolor=$inbound>" . $trip->execution . "</td>";
            echo "<td bgcolor='#00bfff' hidden id='people". $trip->id ."' bgcolor=$inbound>" . $trip->execution . "</td>";
            echo "</tr>";

            /*echo "<td>" . $task['title'] . "</td>";
            echo "<td>" . $task['description'] . "</td>";
            */
        }
        ?>
    </table>
</div>

<script>
    function showDock(link){
        var http = new XMLHttpRequest();
        var url = link;
        var params = '';
        http.open('POST', url, true);

        //Send the proper header information along with the request
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var v=JSON.parse(http.responseText), s="";

                s+="id: "+v.id;
                s+="\nname: "+v.name;
                s+="\ndescription: "+v.description;
                alert(s);
            }
        }
        http.send(params);
    }

    function showShip(link){
        var http = new XMLHttpRequest();
        var url = link;
        var params = '';
        http.open('POST', url, true);

        //Send the proper header information along with the request
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var v=JSON.parse(http.responseText), s="";

                s+="id: "+v.id;
                s+="\nname: "+v.name;
                s+="\nhullCode: "+v.hullCode;
                s+="\ncountry: "+v.countryId;
                alert(s);
            }
        }
        http.send(params);
    }
</script>