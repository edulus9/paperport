<h1><img src="../../../images/cargo.png" height="45"> Cargos</h1>
<div class="row col-md-12 centered">
    <a href="/PaperPort/cargo/create/" class="btn btn-primary btn-xs pull-right"><b>+</b></a>

    <table class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>Notes</th>
            <th>TripId</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
        require_once("../Models/Cargo.php");
        foreach ($cargos as $cargo)
        {
            echo '<tr >';
            echo "<td>" . $cargo->id . "</td>";
            echo "<td>" . $cargo->notes . "</td>";
            echo "<td>" . $cargo->tripId . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/cargo/update/" . $cargo->id . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/PaperPort/cargo/delete/" . $cargo->id . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>