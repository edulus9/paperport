<h1>Docks</h1>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
        require_once("../Models/Ship.php");
        require_once("../Models/Dock.php");
        $inbound="";
        foreach ($docks as $dock)
        {
            echo '<tr >';
            echo "<td>" . $dock->id . "</td>";
            echo "<td>" . $dock->name . "</td>";
            echo "<td>" . $dock->description . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/dock/update/" . $dock->id . "' ><span class='glyphicon glyphicon-update'></span> Edit</a> </td>";
            echo "</tr>";

            /*echo "<td>" . $task['title'] . "</td>";
            echo "<td>" . $task['description'] . "</td>";
            */
        }
        ?>
    </table>
</div>