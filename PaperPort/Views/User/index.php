<h1><img src="../../../images/id.png" height="45"> Users</h1>
<div class="row col-md-12 centered">
    <a href="/PaperPort/user/register/" class="btn btn-primary btn-xs pull-right"><b>+</b></a>

    <table class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
        </thead>
        <?php
        require_once("../Models/User.php");
        $inbound="";
        foreach ($users as $user)
        {
            echo '<tr >';
            echo "<td>" . $user->id . "</td>";
            echo "<td>" . $user->name . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/user/update/" . $user->id . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/PaperPort/user/delete/" . $user->id . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>