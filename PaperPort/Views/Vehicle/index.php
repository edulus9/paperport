<h1><img src="../../../images/vehicle.png" height="45"> Vehicles</h1>
<div class="row col-md-12 centered">
    <a href="/PaperPort/vehicle/create/" class="btn btn-primary btn-xs pull-right"><b>+</b></a>

    <table class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>Brand</th>
            <th>Model</th>
            <th>VIN</th>
            <th>CargoId</th>
            <th>Worth</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
        require_once("../Models/Vehicle.php");
        $inbound="";
        foreach ($vehicles as $vehicle)
        {
            echo '<tr >';
            echo "<td>" . $vehicle->id . "</td>";
            echo "<td>" . $vehicle->brand . "</td>";
            echo "<td>" . $vehicle->model . "</td>";
            echo "<td>" . $vehicle->vin . "</td>";
            echo "<td>" . $vehicle->cargoId . "</td>";
            echo "<td>" . $vehicle->worth . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/vehicle/update/" . $vehicle->id . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/PaperPort/vehicle/delete/" . $vehicle->id . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>