<h1>Edit vehicle</h1>
<form method='post' action='#'>
    <script>

    </script>
    <div class="form-group">
        <label for="title">brand</label>
        <input type="text" class="form-control" id="brand" placeholder="Enter brand" name="brand" value ="<?php if (isset($vehicle->brand)) echo $vehicle->brand;?>">
    </div>
    <div class="form-group">
        <label for="title">model</label>
        <input type="text" class="form-control" id="model" placeholder="Enter model" name="model" value ="<?php if (isset($vehicle->model)) echo $vehicle->model;?>">
    </div>
    <div class="form-group">
        <label for="title">vin</label>
        <input type="text" class="form-control" id="vin" placeholder="Enter vin" name="vin" value ="<?php if (isset($vehicle->vin)) echo $vehicle->vin;?>">
    </div>
    <div class="form-group">
        <label for="title">cargoId</label>
        <input type="text" class="form-control" id="cargoId" placeholder="Enter cargoId" name="cargoId" value ="<?php if (isset($vehicle->cargoId)) echo $vehicle->cargoId;?>">
    </div>
    <div class="form-group">
        <label for="title">worth</label>
        <input type="text" class="form-control" id="worth" placeholder="Enter worth" name="worth" value ="<?php if (isset($vehicle->worth)) echo $vehicle->worth;?>">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    <div id="subt">
    </div>

</form>