<h1><img src="../../../images/user.png" height="45"> Travelers</h1>
<div class="row col-md-12 centered">
    <a href="/PaperPort/traveler/create/" class="btn btn-primary btn-xs pull-right"><b>+</b></a>

    <table class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Nation</th>
            <th>DocId</th>
            <th>CargoId</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
        require_once("../Models/Traveler.php");
        $inbound="";
        foreach ($travelers as $traveler)
        {
            echo '<tr >';
            echo "<td>" . $traveler->id . "</td>";
            echo "<td>" . $traveler->name . "</td>";
            echo "<td>" . $traveler->surname . "</td>";
            echo "<td>" . $traveler->countryId . "</td>";
            echo "<td>" . $traveler->docId . "</td>";
            echo "<td>" . $traveler->cargoId . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/traveler/update/" . $traveler->id . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/PaperPort/traveler/delete/" . $traveler->id . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>