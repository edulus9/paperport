<!doctype html>
<head>
    <meta charset="utf-8">

    <title>PaperPort TOS</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">


    <style>
        body {
            padding-top: 5rem;
        }
        .starter-template {
            padding: 3rem 1.5rem;
            text-align: center;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="/PaperPort/trip/calendar">PaperPort TOS</a>
    <div>
        <?php if(isset($_SESSION["authName"])) echo "<button disabled class='btn btn-info btn-xs'>Hello, <i>".$_SESSION["authName"]."</i></button>";?>
    </div>
    <?php
    if(isset($_SESSION["id"])){
        echo "<a href=\"/PaperPort/user/exit/\"><button class=\"navbar-toggler\" type=\"button\" ><b>Exit</b></button></a>";
    }
    ?>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/trip/index">Trips <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/trip/calendar">Calendar <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/ship/index">Ships <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/traveler/index">Travelers <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/container/index">Containers <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/vehicle/index">Vehicles <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/cargo/index">Cargos <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/dock/index">Docks <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/PaperPort/user/index">Users <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
    <?php
    if(isset($_SESSION["id"])){
        echo "<a href=\"/PaperPort/user/exit/\"><button class='btn btn-info btn-xs' type=\"button\" ><b>Exit</b></button></a>";
    }
    ?>
</nav>

<main role="main" class="container">

    <div class="starter-template">

        <?php
        echo $content_for_layout;
        ?>

    </div>

</main>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
