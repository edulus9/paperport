<h1><img src="../../../images/container.png" height="45"> Containers</h1>
<div class="row col-md-12 centered">
    <a href="/PaperPort/container/create/" class="btn btn-primary btn-xs pull-right"><b>+</b></a>

    <table class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>ContainerId</th>
            <th>CargoId</th>
            <th>Description</th>
            <th>Worth</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
        $inbound="";
        foreach ($containers as $container)
        {
            echo '<tr >';
            echo "<td>" . $container->id . "</td>";
            echo "<td>" . $container->containerId . "</td>";
            echo "<td>" . $container->cargoId . "</td>";
            echo "<td>" . $container->description . "</td>";
            echo "<td>" . $container->worth . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/container/update/" . $container->id . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/PaperPort/container/delete/" . $container->id . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>