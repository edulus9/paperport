<h1><img src="../../../images/ship.svg" height="45"> Ships</h1>
<div class="row col-md-12 centered">
    <a href="/PaperPort/ship/create/" class="btn btn-primary btn-xs pull-right"><b>+</b></a>

    <table class="table table-striped custab">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Nation</th>
            <th>HullCode</th>
            <th>Added</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
        require_once("../Models/Ship.php");
        require_once("../Models/Ship.php");
        $inbound="";
        foreach ($ships as $ship)
        {
            echo '<tr >';
            echo "<td>" . $ship->id . "</td>";
            echo "<td>" . $ship->name . "</td>";
            echo "<td>" . $ship->countryId . "</td>";
            echo "<td>" . $ship->hullCode . "</td>";
            echo "<td>" . $ship->added . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/PaperPort/ship/update/" . $ship->id . "' ><span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/PaperPort/ship/delete/" . $ship->id . "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>